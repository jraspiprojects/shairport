sudo apt-get install git libao-dev libssl-dev libcrypt-openssl-rsa-perl libio-socket-inet6-perl libwww-perl avahi-utils libmodule-build-perl
cd $HOME
mkdir tmp
git clone https://github.com/njh/perl-net-sdp.git
cd perl-net-sdp
perl Build.PL
sudo ./Build
sudo ./Build test
sudo ./Build install
cd ../../
mkdir shairport
echo "cd shairport && ./shairport.pl -a $(hostname)" > start
chmod +x start
git clone https://github.com/jraspiprojects/shairport.git
cd shairport 
make
echo "Now ready."
cd ../
echo "Use start file to start shairport"
